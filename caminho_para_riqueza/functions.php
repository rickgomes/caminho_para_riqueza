<?php
// ADICIONANDO TÍTULO AO TEMPLATE
add_theme_support('title-tag');

// ADICIONANDO CUSTOM POST TYPE PARA VÍDEOS
require_once('includes/post-type/post_type_videos.php');

// ADICIONANDO CUSTOM POST TYPE PARA E-BOOKS
require_once('includes/post-type/post_type_ebooks.php');

// ADICIONANDO CUSTOM POST TYPE PARA CURSOS ONLINE
require_once('includes/post-type/post_type_cursos.php');

// ADICIONANDO CUSTOM POST TYPE PARA CURSOS WEBINARS
require_once('includes/post-type/post_type_webinars.php');

// ADICIONANDO CUSTOM POST TYPE PARA CURSOS INFOGRÁFICOS
require_once('includes/post-type/post_type_infograficos.php');

//ADICIONANDO SCRIPT PARA MENU DROPDOWN BOOTSTRAP
require_once('includes/menu/wp-bootstrap-navwalker.php');

// PLUGIN - ACF - DEFININDO PATH E URL PARA O PLUGIN
define( 'MY_ACF_PATH', get_stylesheet_directory() . '/includes/acf/' );
define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/includes/acf/' );

// PLUGIN - ACF - ADICIONANDO PLUGIN AO TEMA
include_once( MY_ACF_PATH . 'acf.php' );

// PLUGIN - ACF - CUSTOMIZANDO URL DE CONFIGURAÇÕES PARA CORRIGIR URL INCORRETA
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {
    return MY_ACF_URL;
}

// PLUGIN - ACF - REMOVENDO OPÇÃO DO PAINEL DE CONTROLE
// add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
// function my_acf_settings_show_admin( $show_admin ) {
//     return false;
// }

// PLUGIN - ACF - ADICIONANDO ROTA PARA SALVAR CONFIGURAÇÕES
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/includes/acf-config';
    return $path;
}

// PLUGIN - ACF - ADICIONANDO ROTA PARA CARREGAR CONFIGURAÇÕES
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/includes/acf-config';
    return $paths;
}

//ADICIONANDO SCRIPTS NECESSÁRIOS PARA FRONT
function scripts_front() {
  wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '4.0.0', true );
  wp_register_script('bootstrap-toolkit', get_template_directory_uri() . '/js/bootstrap-toolkit.min.js', array('jquery'), '4.0.0', true );
  wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true );

	wp_register_style ('style', get_template_directory_uri() . '/css/style.min.css', array(), '1.0.0', 'all', true );
	
  wp_enqueue_script('bootstrap');
  wp_enqueue_script('bootstrap-toolkit');
  wp_enqueue_script('scripts');

	wp_enqueue_style ('style');
}
add_action('wp_enqueue_scripts', 'scripts_front');

//ADICIONANDO SCRIPTS NECESSÁRIOS PARA ADMIN
add_action( 'admin_enqueue_scripts', 'scripts_admin' );
add_action( 'login_enqueue_scripts', 'scripts_admin' );
function scripts_admin() {
  wp_register_style ( 'custom_admin', get_template_directory_uri() . '/css/admin/admin.min.css', array(), '1.0.0', 'all', false );

  wp_enqueue_style ( 'custom_admin' );
}


//ADICIONANDO MENUS
add_theme_support('menus');
register_nav_menus(
  array(
    'menu_principal'=>'Menu Principal',
    'menu_topo'=>'Menu Topo Cabeçalho',
));

// ADICIONANDO SUPORTE A THUMBNAILS
add_action('after_setup_theme', 'makers_thumbs');
function makers_thumbs(){
  if (function_exists('add_theme_support')) {
    global $makers_data;
    add_theme_support( 'post-thumbnails' );
    add_image_size('blog_cover', 2000, 600, true);
    add_image_size('thumb_box', 600, 600, true);
    add_image_size('thumb_post', 260, 260, true);
    add_image_size('blog_cover', 1700, 420, true);
    add_image_size('call_to_action', 700, 260, true);
  }
}

//REGISTRANDO SIDEBAR DO BLOG E MAPA DO SITE
function adicionando_sidebar() {
  global $_wp_sidebars_widgets;
  if ( empty( $_wp_sidebars_widgets ) ) :
      $_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
  endif;

  $sidebars_widgets_count = $_wp_sidebars_widgets;
  $numero_colunas = count( $sidebars_widgets_count[ 'box_menu_rodape' ] );
  $display = '';
  $erro_rodape = "";

  if($numero_colunas == 1){
      $tamanho_mobile = '12';
      $tamanho_coluna = '12';
  } elseif($numero_colunas == 2){
      $tamanho_mobile = '6';
      $tamanho_coluna = '6';
  } elseif($numero_colunas == 3){
      $tamanho_mobile = '4';
      $tamanho_coluna = '4';
  } elseif($numero_colunas == 4){
      $tamanho_mobile = '3';
      $tamanho_coluna = '3';
  } elseif($numero_colunas == 5){
      $tamanho_mobile = '4';
      $tamanho_coluna = '2';
  } elseif($numero_colunas == 6){
      $tamanho_mobile = '4';
      $tamanho_coluna = '2';
  } elseif($numero_colunas > 7){
      $tamanho_mobile = '2';
      $tamanho_coluna = '2';
      $display = 'd-none';
      $erro_rodape = "<div class='alert alert-danger'><strong>ERRO!</strong> Número de colunas maior que 6!</div>";
  }
  register_sidebar(array(
    'name'          => __( 'Home', 'home_sidebar' ),
    'id'            => 'home_sidebar',
    'description'   => __( 'Área para inserir widgets para sidebar da home.', 'home_sidebar' ),
    'before_widget' => '<div id="%1$s" class="widget widget_home mb-5 %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h4 class="titulo_sessao d-block mb-3">',
    'after_title'   => '</h4>',
  ));
  register_sidebar(array(
    'name'          => __( 'Blog Sidebar', 'blog_sidebar' ),
    'id'            => 'blog_sidebar',
    'description'   => __( 'Área para inserir widgets para sidebar do blog.', 'blog_sidebar' ),
    'before_widget' => '<div id="%1$s" class="widget widget_blog mb-5 %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h4 class="titulo_sessao d-block mb-3">',
    'after_title'   => '</h4>',
  ));
  register_sidebar(array(
    'name'          => __( 'Rodapé', 'menu_rodape' ),
    'id'            => 'box_menu_rodape',
    'class'         => 'row py-4',
    'description'   => __( 'Área para adicionar os menus personalizados no rodapé.', 'menu_rodape' ),
    'before_title'  => '<h6 class="titulo">',
    'after_title'   => '</h6>',
    'before_widget' => '<div id="%1$s" class="col-'.$tamanho_mobile.' col-lg-'.$tamanho_coluna.' '.$display.'">',
    'after_widget'  => '</div>'.$erro_rodape.''
  ));
}
add_action('widgets_init','adicionando_sidebar');

//LIMITANDO CARACTERES NO RESUMO
function resumo($caracteres) {
  $resumo = get_the_excerpt();
  $caracteres++;

  if ( mb_strlen( $resumo ) > $caracteres ) {
    $subex = mb_substr( $resumo, 0, $caracteres - 7 );
    $exwords = explode(' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo mb_substr( $subex, 0, $excut );
    } else {
      echo $subex;
    }
    echo '...';
  } else {
    echo $resumo;
  }
}

// LIMITANDO CARACTERES DO TÍTULO
function titulo($caracteres) {
  $titulo = get_the_title();
  $caracteres++;

  if ( mb_strlen( $titulo ) > $caracteres ) {
    $subex = mb_substr( $titulo, 0, $caracteres - 7 );
    $exwords = explode(' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo mb_substr( $subex, 0, $excut );
    } else {
      echo $subex;
    }
    echo '...';
  } else {
    echo $titulo;
  }
}
 
//ADICIONANDO ASSINATURA NO BACKEND
function remove_footer_admin () {
	echo 'Desenvolvido por <a href="http://www.manymakers.digital" target="_blank">Many Makers</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

//REMOVENDO VERSÃO DO WP DO RODAPÉ - BACKEND
function change_footer_version() {
	return 'Versão 2.0';
}
add_filter( 'update_footer', 'change_footer_version', 9999 );

//RETIRAR LOGO DO WP DA BARRA SUPERIOR
function wps_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('about');
	$wp_admin_bar->remove_menu('wporg');
	$wp_admin_bar->remove_menu('documentation');
	$wp_admin_bar->remove_menu('support-forums');
	$wp_admin_bar->remove_menu('feedback');
	$wp_admin_bar->remove_menu('view-site');
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

//MOSTRAR BARRA SUPERIOR SOMENTE PARA ADMINSTRADOR
// add_filter('show_admin_bar', '__return_false');

//ADICIONANDO PÁGINA DE CUSTOMIZAÇÕES DO TEMA
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title'      => 'Template CPR',
    'menu_title'      => 'Template CPR',
    'menu_slug'       => 'template_cpr',
    'parent_slug'     => '',
    'capability'      => 'edit_posts',
    'icon_url'        => 'dashicons-layout',
    'redirect'        => true,
  ));

  acf_add_options_sub_page(array(
		'page_title' 	=> 'Geral',
		'menu_title'	=> 'Geral',
    'parent_slug'	=> 'template_cpr',
    'update_button'		=> __('Atualizar', 'acf'),
    'updated_message'	=> __("Configurações atualizadas com sucesso.", 'acf'),
  ));
  
  acf_add_options_sub_page(array(
		'page_title' 	=> 'Home',
		'menu_title'	=> 'Home',
    'parent_slug'	=> 'template_cpr',
    'update_button'		=> __('Atualizar', 'acf'),
    'updated_message'	=> __("Configurações atualizadas com sucesso.", 'acf'),
	));
}

// PERMITINDO SVG NO UPLOAD DE IMAGENS
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');