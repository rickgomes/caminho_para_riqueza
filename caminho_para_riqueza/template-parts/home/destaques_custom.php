<?php $nome_sessao = get_field('sessao_destaques', 'option'); ?>
<?php $anuncio = get_field('banner_destaque', 'option'); ?>

<?php $destaque_01 = get_field('destaque_01', 'option'); ?>
    <?php $destaque_01_legenda = get_field('destaque_01_legenda', 'option'); ?>
    <?php $destaque_01_titulo = get_field('destaque_01_titulo', 'option'); ?>
    <?php $destaque_01_link = get_field('destaque_01_link', 'option'); ?>
    <?php $destaque_01_config = get_field('destaque_01_config', 'option'); ?>
<?php $destaque_02 = get_field('destaque_02', 'option'); ?>
    <?php $destaque_02_legenda = get_field('destaque_02_legenda', 'option'); ?>
    <?php $destaque_02_titulo = get_field('destaque_02_titulo', 'option'); ?>
    <?php $destaque_02_link = get_field('destaque_02_link', 'option'); ?>
    <?php $destaque_02_config = get_field('destaque_02_config', 'option'); ?>
<?php $destaque_03 = get_field('destaque_03', 'option'); ?>
    <?php $destaque_03_legenda = get_field('destaque_03_legenda', 'option'); ?>
    <?php $destaque_03_titulo = get_field('destaque_03_titulo', 'option'); ?>
    <?php $destaque_03_link = get_field('destaque_03_link', 'option'); ?>
    <?php $destaque_03_config = get_field('destaque_03_config', 'option'); ?>
<?php $destaque_04 = get_field('destaque_04', 'option'); ?>
    <?php $destaque_04_legenda = get_field('destaque_04_legenda', 'option'); ?>
    <?php $destaque_04_titulo = get_field('destaque_04_titulo', 'option'); ?>
    <?php $destaque_04_link = get_field('destaque_04_link', 'option'); ?>
    <?php $destaque_04_config = get_field('destaque_04_config', 'option'); ?>
<?php $destaque_05 = get_field('destaque_05', 'option'); ?>
    <?php $destaque_05_legenda = get_field('destaque_05_legenda', 'option'); ?>
    <?php $destaque_05_titulo = get_field('destaque_05_titulo', 'option'); ?>
    <?php $destaque_05_link = get_field('destaque_05_link', 'option'); ?>
    <?php $destaque_05_config = get_field('destaque_05_config', 'option'); ?>
<?php $destaque_06 = get_field('destaque_06', 'option'); ?>
    <?php $destaque_06_legenda = get_field('destaque_06_legenda', 'option'); ?>
    <?php $destaque_06_titulo = get_field('destaque_06_titulo', 'option'); ?>
    <?php $destaque_06_link = get_field('destaque_06_link', 'option'); ?>
    <?php $destaque_06_config = get_field('destaque_06_config', 'option'); ?>


<section id="destaques" class="container">
    <div class="row mt-3">
        <?php if(!empty($nome_sessao)) { ?>
            <div class="col-12 d-lg-none">
                <span class="titulo_sessao"><?php echo $nome_sessao; ?></span>
            </div>
        <?php } ?>

        <?php if (empty ($anuncio)) { ?>
            <div id="coluna_01" class="loop_thumb col-12 col-md-8 col-lg-4">
        <?php } else { ?>
            <div id="coluna_01" class="loop_thumb col-12 col-md-8 col-lg-5">
        <?php } ?>

            <div class="row">
                <!-- DESTAQUE 01 -->
                <?php if(!empty($destaque_01)) { ?>
                    <div class="col-12 col-md-6 col-lg-12 mt-3 mt-lg-2 mt-xl-3">
                        <?php if (empty ($anuncio)) { $thumb_small = 'small'; } else { $thumb_small = ''; $thumb_big = ''; } ?> 
                        <div class="itens">
                            <?php if ($destaque_01_config == true) { $destaque_01_config = 'target="_blank"'; } ?>
                            <a href="<?php echo $destaque_01_link; ?>" class="itens" alt="<?php echo $destaque_01_titulo; ?>" <?php echo $destaque_01_config; ?>>
                                <div class="box_texto d-flex flex-column justify-content-start">
                                    <strong><?php echo $destaque_01_legenda; ?></strong>
                                    <span><?php echo $destaque_01_titulo; ?></span>                    
                                </div>
                                <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_01; ?>);">
                                    <div class="hover"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>

                <!-- DESTAQUE 02 -->
                <?php if(!empty($destaque_02)) { ?>
                    <div class="col-12 col-md-6 col-lg-12 mt-3 mt-lg-2 mt-xl-3">
                        <?php if (empty ($anuncio)) { $thumb_small = 'small'; } else { $thumb_small = ''; $thumb_big = ''; } ?> 
                        <div class="itens">
                            <?php if ($destaque_02_config == true) { $destaque_02_config = 'target="_blank"'; } ?>
                            <a href="<?php echo $destaque_02_link; ?>" class="itens" alt="<?php echo $destaque_02_titulo; ?>" <?php echo $destaque_02_config; ?>>
                                <div class="box_texto d-flex flex-column justify-content-start">
                                    <strong><?php echo $destaque_02_legenda; ?></strong>
                                    <span><?php echo $destaque_02_titulo; ?></span>                    
                                </div>
                                <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_02; ?>);">
                                    <div class="hover"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        
        <!-- DESTAQUE 03 -->
        <?php if (empty ($anuncio)) { ?>
            <div id="coluna_02" class="loop_thumb col-12 col-md-4 col-lg-8">
        <?php } else { ?>
            <div id="coluna_02" class="loop_thumb col-12 col-md-4 col-lg-7">
        <?php } ?>

            <?php if (!empty ($anuncio)) { ?>
                <!-- ANÚNCIO -->
                <div class="col-12 mt-2 mt-lg-1 mt-xl-2 mb-3 mb-lg-4 mb-xl-3 d-none d-lg-block px-0">
                    <?php $anuncio_config = get_field('banner_destaque_config', 'option'); ?>
                    <?php if ($anuncio_config == true) { $anuncio_config = 'target="_blank"'; } ?>
                    <a href="<?php the_field ('banner_destaque_link', 'option'); ?>" <?php echo $anuncio_config; ?>>
                        <img src="<?php echo $anuncio['url']; ?>" class="img-fluid">
                    </a>
                </div>
            <?php } ?>

            <?php if (empty ($anuncio)) { ?>
                <div class="col-12 mt-3 mt-lg-2 mt-xl-3 px-0">
            <?php } else { ?>
                <div class="col-12 mt-3 mt-lg-2 mt-xl-3 px-0">
            <?php } ?>
                
                <?php if(!empty($destaque_03)) { ?>
                    <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = 'big'; } else { $thumb_big = 'big'; } ?>
                    <div class="itens">
                        <?php if ($destaque_03_config == true) { $destaque_03_config = 'target="_blank"'; } ?>
                        <a href="<?php echo $destaque_03_link; ?>" class="itens" alt="<?php echo $destaque_03_titulo; ?>" <?php echo $destaque_03_config; ?>>
                            <div class="box_texto d-flex flex-column justify-content-start">
                                <strong><?php echo $destaque_03_legenda; ?></strong>
                                <span><?php echo $destaque_03_titulo; ?></span>                    
                            </div>
                            <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_03; ?>);">
                                <div class="hover"></div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row mt-3 mt-md-4 loop_thumb">
        <!-- DESTAQUE 04 -->
        <?php if(!empty($destaque_04)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = ''; } else { $thumb_small = ''; $thumb_big = ''; } ?>
                <div class="itens">
                    <?php if ($destaque_04_config == true) { $destaque_04_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $destaque_04_link; ?>" class="itens" alt="<?php echo $destaque_04_titulo; ?>" <?php echo $destaque_04_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $destaque_04_legenda; ?></strong>
                            <span><?php echo $destaque_04_titulo; ?></span>                    
                        </div>
                        <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_04; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <!-- DESTAQUE 05 -->
        <?php if(!empty($destaque_05)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = ''; } else { $thumb_small = ''; $thumb_big = ''; } ?>
                <div class="itens">
                    <?php if ($destaque_05_config == true) { $destaque_05_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $destaque_05_link; ?>" class="itens" alt="<?php echo $destaque_05_titulo; ?>" <?php echo $destaque_05_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $destaque_05_legenda; ?></strong>
                            <span><?php echo $destaque_05_titulo; ?></span>                    
                        </div>
                        <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_05; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <!-- DESTAQUE 06 -->
        <?php if(!empty($destaque_06)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = ''; } else { $thumb_small = ''; $thumb_big = ''; } ?>
                <div class="itens">
                    <?php if ($destaque_06_config == true) { $destaque_06_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $destaque_06_link; ?>" class="itens" alt="<?php echo $destaque_06_titulo; ?>" <?php echo $destaque_06_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $destaque_06_legenda; ?></strong>
                            <span><?php echo $destaque_06_titulo; ?></span>                    
                        </div>
                        <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php echo $destaque_06; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
