<?php query_posts(
        array(
            'posts_per_page'=> '5', 
            'orderby' => 'date', 
            'order' => 'DESC', 
            'post_type' => 'post', 
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key'     => 'destaque',
                    'value'   => '1',
                    'compare' => '!='
                ),
                array(
                    'key'     => 'destaque',
                    'compare' => 'NOT EXISTS',
                )
))); ?>
<?php if (have_posts()) : ?> 
    <section id="box_noticias" class="container my-5">
        <div class="row">

            <!-- ÚLTIMAS NOTÍCIAS -->
            <div id="ultimas_blog" class="loop_thumb_post col-12 col-md-7 col-lg-8">
                <?php $sessao_ultimas = get_field('sessao_ultimas', 'option'); ?>
                <?php if(!empty($sessao_ultimas)) { ?>
                    <h3 class="titulo_sessao d-block mb-4"><?php echo $sessao_ultimas; ?></h3>
                <?php } ?>
                
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part( 'template-parts/content', 'post' ); ?>
                <?php endwhile; ?>

                <?php $botao_ver_todas = get_field('botao_ver_todas', 'option'); ?>
                <?php if ($botao_ver_todas == true) { ?>
                    <?php $botao_ver_todas_texto = get_field('botao_ver_todas_texto', 'option'); ?>
                    <?php $botao_ver_todas_link = get_field('botao_ver_todas_link', 'option'); ?>
                        <a href="<?php echo $botao_ver_todas_link; ?>" class="bt_blog d-block mx-auto my-4" alt="<?php $botao_ver_todas_texto; ?>" title="<?php echo $botao_ver_todas_texto; ?>">
                            <?php echo $botao_ver_todas_texto; ?>
                        </a>
                <?php } ?>
            </div>
            
            <!-- SIDEBAR -->
            <?php if (is_active_sidebar('home_sidebar')) : ?>
                <div id="home_sidebar" class="col-12 col-md-5 col-lg-4 sidebar mt-5 mt-md-0 pt-lg-5">
                    <?php dynamic_sidebar('home_sidebar'); ?>
                </div>
            <?php endif; ?>

        </div>
    </section>
<?php endif; ?>
<?php wp_reset_query(); ?>