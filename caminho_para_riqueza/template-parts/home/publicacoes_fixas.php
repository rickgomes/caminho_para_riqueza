<?php $fixa_01 = get_field('fixa_01', 'option'); ?>
<?php $fixa_01_legenda = get_field('fixa_01_legenda', 'option'); ?>
<?php $fixa_01_titulo = get_field('fixa_01_titulo', 'option'); ?>
<?php $fixa_01_link = get_field('fixa_01_link', 'option'); ?>
<?php $fixa_01_config = get_field('fixa_01_config', 'option'); ?>

<?php $fixa_02 = get_field('fixa_02', 'option'); ?>
<?php $fixa_02_legenda = get_field('fixa_02_legenda', 'option'); ?>
<?php $fixa_02_titulo = get_field('fixa_02_titulo', 'option'); ?>
<?php $fixa_02_link = get_field('fixa_02_link', 'option'); ?>
<?php $fixa_02_config = get_field('fixa_02_config', 'option'); ?>

<?php $fixa_03 = get_field('fixa_03', 'option'); ?>
<?php $fixa_03_legenda = get_field('fixa_03_legenda', 'option'); ?>
<?php $fixa_03_titulo = get_field('fixa_03_titulo', 'option'); ?>
<?php $fixa_03_link = get_field('fixa_03_link', 'option'); ?>
<?php $fixa_03_config = get_field('fixa_03_config', 'option'); ?>

<section id="destaques_fixos" class="container my-5">
    <div class="row loop_thumb azul">
        <?php $sessao_fixas = get_field('sessao_fixas', 'option'); ?>
        <?php if(!empty($sessao_fixas)) { ?>
            <div class="col-12 mb-3">
                <h3 class="titulo_sessao"><?php echo $sessao_fixas; ?></h3>
            </div>
        <?php } ?>
        
        <!-- FIXA 01 -->
        <?php if(!empty($fixa_01)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <div class="itens">
                    <?php if ($fixa_01_config == true) { $fixa_01_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $fixa_01_link; ?>" class="itens" alt="<?php echo $fixa_01_titulo; ?>" <?php echo $fixa_01_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $fixa_01_legenda; ?></strong>
                            <h4><?php echo $fixa_01_titulo; ?></h4>                    
                        </div>
                        <div class="box_img" style="background-image: url(<?php echo $fixa_01['url']; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
            
        <!-- FIXA 02 -->
        <?php if(!empty($fixa_02)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <div class="itens">
                    <?php if ($fixa_02_config == true) { $fixa_02_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $fixa_02_link; ?>" class="itens" alt="<?php echo $fixa_02_titulo; ?>" <?php echo $fixa_02_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $fixa_02_legenda; ?></strong>
                            <h4><?php echo $fixa_02_titulo; ?></h4>                    
                        </div>
                        <div class="box_img" style="background-image: url(<?php echo $fixa_02['url']; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>

        <!-- FIXA 03 -->
        <?php if(!empty($fixa_03)) { ?>
            <div class="col-12 col-md-4 mb-3 mb-lg-0">
                <div class="itens">
                    <?php if ($fixa_03_config == true) { $fixa_03_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $fixa_03_link; ?>" class="itens" alt="<?php echo $fixa_03_titulo; ?>" <?php echo $fixa_03_config; ?>>
                        <div class="box_texto d-flex flex-column justify-content-start">
                            <strong><?php echo $fixa_03_legenda; ?></strong>
                            <h4><?php echo $fixa_03_titulo; ?></h4>                    
                        </div>
                        <div class="box_img" style="background-image: url(<?php echo $fixa_03['url']; ?>);">
                            <div class="hover"></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>

    </div>
</section>
