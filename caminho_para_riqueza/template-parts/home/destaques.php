<?php $nome_sessao = get_field('sessao_destaques', 'option'); ?>
<?php $anuncio = get_field('banner_destaque', 'option'); ?>

<section id="destaques" class="container">
    <div class="row mt-3">
        <?php if(!empty($nome_sessao)) { ?>
            <div class="col-12 d-lg-none">
                <span class="titulo_sessao"><?php echo $nome_sessao; ?></span>
            </div>
        <?php } ?>
        <?php query_posts(array('posts_per_page'=> '2', 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'post', 'meta_key' => 'destaque', 'meta_value' => true)); ?>
        <?php if (have_posts()) : ?>
            <?php if (empty ($anuncio)) { ?>
                <div id="coluna_01" class="loop_thumb col-12 col-md-8 col-lg-4">
            <?php } else { ?>
                <div id="coluna_01" class="loop_thumb col-12 col-md-8 col-lg-5">
            <?php } ?>

                <div class="row">
                <?php while (have_posts()) : the_post(); ?>
                    <div class="col-12 col-md-6 col-lg-12 mt-3 mt-lg-2 mt-xl-3">
                        <?php if (empty ($anuncio)) { $thumb_small = 'small'; } else { $thumb_small = ''; $thumb_big = ''; } ?> 
                        <?php include( locate_template( 'template-parts/content-thumb.php', false, false ) ); ?>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>

        <?php query_posts(array('posts_per_page'=> '1', 'offset' => '2', 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'post', 'meta_key' => 'destaque', 'meta_value' => true)); ?>
        <?php if (have_posts()) : ?>
            <?php if (empty ($anuncio)) { ?>
                <div id="coluna_02" class="loop_thumb col-12 col-md-4 col-lg-8">
            <?php } else { ?>
                <div id="coluna_02" class="loop_thumb col-12 col-md-4 col-lg-7">
            <?php } ?>

                <?php while (have_posts()) : the_post(); ?>
                    <?php if (!empty ($anuncio)) { ?>
                        <!-- ANÚNCIO -->
                        <div class="col-12 mt-2 mt-lg-1 mt-xl-2 mb-3 mb-lg-4 mb-xl-3 d-none d-lg-block px-0">
                            <?php $anuncio_config = get_field('banner_destaque_config', 'option'); ?>
                            <?php if ($anuncio_config == true) { $anuncio_config = 'target="_blank"'; } ?>
                            <a href="<?php the_field ('banner_destaque_link', 'option'); ?>" <?php echo $anuncio_config; ?>>
                                <img src="<?php echo $anuncio['url']; ?>" class="img-fluid">
                            </a>
                        </div>
                    <?php } ?>

                    <!-- POST BIG -->
                    <?php if (empty ($anuncio)) { ?>
                        <div class="col-12 mt-3 mt-lg-2 mt-xl-3 px-0">
                    <?php } else { ?>
                        <div class="col-12 mt-3 mt-lg-2 mt-xl-3 px-0">
                    <?php } ?>
                        <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = 'big'; } else { $thumb_big = 'big'; } ?>
                        <?php include( locate_template( 'template-parts/content-thumb.php', false, false ) ); ?> 
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>

    <?php query_posts(array('posts_per_page'=> '3', 'offset' => '3', 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'post', 'meta_key' => 'destaque', 'meta_value' => true)); ?>
    <?php if (have_posts()) : ?>
        <div class="row mt-3 mt-md-4 loop_thumb">
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-12 col-md-4 mb-3 mb-lg-0">
                    <?php if (empty ($anuncio)) { $thumb_small = ''; $thumb_big = ''; } else { $thumb_small = ''; $thumb_big = ''; } ?>
                    <?php include( locate_template( 'template-parts/content-thumb.php', false, false ) ); ?>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
</section>
