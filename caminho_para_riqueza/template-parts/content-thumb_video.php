<div class="itens">
    <a href="<?php the_permalink(); ?>" class="itens" alt="<?php the_title(); ?>">
        <div class="box_texto d-flex flex-column justify-content-start">
            <?php $nome_categoria = get_the_category($post->ID); ?>
            <?php if (empty($nome_categoria)) { ?>
                <?php $post_type_slug = get_post_type($post->ID); ?>
                <?php $post_type = get_post_type_object($post_type_slug); ?>
                <strong><?php echo $post_type->labels->name; ?></strong>
            <?php } else { ?>
                <strong><?php echo $nome_categoria[0]->cat_name; ?></strong>
            <?php } ?>
            <h4><?php the_title();?></h4>                    
        </div>
        <div class="box_img <?php echo $thumb_big; ?> <?php echo $thumb_small; ?>" style="background-image: url(<?php the_post_thumbnail_url('thumb_box'); ?>);">
            <div class="hover"></div>
        </div>
    </a>
</div>