<a href="<?php the_permalink(); ?>" class="post_item row mx-0 mb-5" alt="<?php the_title(); ?>">
    <div class="img px-0 col-5 col-md-4 col-lg-3">
        <div class="box_img" style="background-image: url(<?php the_post_thumbnail_url('thumb_post'); ?>);">
            <div class="hover"></div>
        </div>
    </div>
    <div class="infos col-7 col-md-8 col-lg-9">
        <div class="cat_date">
            <?php $nome_categoria = get_the_category($post->ID); ?>
            <span><?php echo $nome_categoria[0]->cat_name; ?> • </span>
            <?php echo get_the_date('d')?> de <?php echo get_the_date('F')?> de <?php echo get_the_date('Y')?>
        </div> 
        <h4 class="titulo"><?php the_title(); ?></h4>
        <p class="desc mt-3 d-none d-lg-block"><?php echo resumo(150);?></p>
    </div>
</a>