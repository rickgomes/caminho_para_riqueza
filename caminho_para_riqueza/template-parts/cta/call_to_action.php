<?php

	// PEGANDO TODAS AS VARIÁRVEIS
	$formato = get_field('formato');

	$imagem_padrao = get_field('imagem_padrao');
	$titulo_padrao = get_field('titulo_padrao');
	$subtitulo_padrao = get_field('subtitulo_padrao');
	$texto_botao = get_field('texto_botao');

	$imagem_personalizado = get_field('imagem_personalizado');

	$link = get_field('link');
	$cores = get_field('cores');

	$cor_background = get_field('cor_background');
	$cor_borda = get_field('cor_borda');
	$cor_titulo = get_field('cor_titulo');
	$cor_texto = get_field('cor_texto');

	$cor_background_botao = get_field('cor_background_botao');
	$cor_background_botao_ativo = get_field('cor_background_botao_ativo');
	$cor_texto_botao = get_field('cor_texto_botao');

	//CONDICIONAIS PARA EXIBIÇÃO DAS CORES
	if ($cores == 'Sim') {
		$cor_background = 'background:'.$cor_background.'';
		$cor_borda = 'border: solid '.$cor_borda.' 2px';
		$cor_titulo = 'color:'.$cor_titulo.'';
		$cor_texto = 'color:'.$cor_texto.'';
		$cor_background_botao = 'background:'.$cor_background_botao.'';
		$cor_background_botao_ativo = 'background:'.$cor_background_botao_ativo.'';
		$cor_texto_botao = 'color:'.$cor_texto_botao.' !important';
	} else {
		$cor_background = 'background:#284468';
		$cor_borda = 'border:solid #FFFFFF 2px';
		$cor_titulo = 'color:#FFFFFF';
		$cor_texto = 'color:#FFFFFF';
		$cor_background_botao = 'background:#B89C6A';
		$cor_background_botao_ativo = 'background:#888888';
		$cor_texto_botao = 'color:#FFFFFF !important';
	}
?>

<?php // PADRÃO | CALL TO ACTION ?>
<?php if ($formato == 'Box Padrão') { ?>

	<!--DEFININDO PROPRIEDADES DE CORES-->
	<style type="text/css" media="screen">
		#call_to_action {
			width: 100%;
			padding:10px;
			margin:50px 0;
			<?php echo $cor_background; ?>;
		}

		#box_padrao {
			width: 100%;
			padding:15px;
            color: #FFFFFF;
            text-align: left;
			<?php echo $cor_borda; ?>;
		}

		#box_padrao #conteudo_box span {
			display: block;
			font-size: 18px;
			font-weight: 700;
			line-height: 25px;
			margin-bottom: 5px;
			text-transform: uppercase;
			<?php echo $cor_titulo; ?>;
		}		

		#box_padrao #conteudo_box p {
			display: block;
			font-size: 14px;
			font-weight: 400;
			line-height: 20px;
			<?php echo $cor_texto; ?>;
			margin-bottom: 5px !important;
		}

		#box_padrao #conteudo_box a {
			display: inline-block;
			font-size: 16px;
			line-height: 25px;
			padding: 5px 10px;
			text-decoration: none;
			font-weight: 700 !important;
			<?php echo $cor_texto_botao; ?>;
			<?php echo $cor_background_botao; ?>;
        }	
        	
		#box_padrao #conteudo_box a:hover {
			text-decoration: none !important;
			<?php echo $cor_texto_botao; ?>;
			<?php echo $cor_background_botao_ativo; ?>;
		}		
	</style>
	
	<!-- BOX PADRÃO -->
	<div id="call_to_action">
		<div id="box_padrao">
			<div id="conteudo_box" class="row">
				<div class="col-12 col-lg-4">
                    <a href="<?php echo $link; ?>" style="background:none;" target="_blank">
                        <?php echo wp_get_attachment_image( $imagem_padrao, 'thumb_box', "", array("class" => "img-fluid")); ?>
                    </a>
				</div>
				<div class="col-12 col-lg-8 mt-3 mt-lg-0">
					<span><?php echo $titulo_padrao; ?></span>
					<p><?php echo $subtitulo_padrao; ?></p>
					<a href="<?php echo $link; ?>" class="mt-3" target="_blank"><?php echo $texto_botao; ?></a>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
	</div>
<?php } ?>



<?php // PERSONALIZADO | CALL TO ACTION ?>
<?php if ($formato == 'Personalizado') { ?>

	<!--DEFININDO PROPRIEDADES DE CORES-->
	<style type="text/css" media="screen">
		#call_to_action { width: 100%; }
	</style>
	
	<!-- BOX PERSONALIZADO -->
	<div id="call_to_action">
		<div id="box_personalizado" class="row">
            <div class="col-12">
                <a href="<?php echo $link; ?>" class="link" target="_blank">
                    <?php echo wp_get_attachment_image( $imagem_personalizado, 'call_to_action', "", array("class" => "img-fluid")); ?>
                </a>
            </div>
		</div>
	</div>
<?php } ?>