<?php $post_id = get_the_ID() ?>
<?php $relacionados = get_field('videos_relacionados'); ?>
<?php $posts_relacionados = array ("video_01" => get_field ('video_relacionado_01'), "video_02" => get_field ('video_relacionado_02'), "video_03" => get_field ('video_relacionado_03')); ?>
<?php $query_selecionados = array('post__in' => $posts_relacionados, 'posts_per_page'=> '3', 'orderby' => 'post__in', 'order' => 'DESC', 'post_type' => 'videos'); ?>
<?php $query_automatica = array('post__not_in' => array ($post_id), 'posts_per_page'=> '3', 'orderby' => 'rand', 'order' => 'DESC', 'post_type' => 'videos'); ?>
<?php if ($relacionados == true) { ?>
    <?php query_posts($query_selecionados); ?>
<?php } else { ?>
    <?php query_posts($query_automatica); ?>
<?php }?>
<?php if (have_posts()) : ?>
    <div id="videos_relacionados" class="my-5">
        <div class="row loop_thumb">
            <div class="col-12 mb-3">
                <span class="titulo_sessao">Vídeos Relacionados</span>
            </div>
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-12 col-md-4 mb-3 mb-lg-0">
                    <?php get_template_part( 'template-parts/content', 'thumb' ); ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>