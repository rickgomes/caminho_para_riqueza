<?php $post_id = get_the_ID() ?>
<?php $relacionados = get_field('posts_relacionados'); ?>
<?php $posts_relacionados = array ("post_01" => get_field ('post_relacionado_01'), "post_02" => get_field ('post_relacionado_02'), "post_03" => get_field ('post_relacionado_03')); ?>
<?php $query_selecionados = array('post__in' => $posts_relacionados, 'posts_per_page'=> '3', 'orderby' => 'post__in', 'order' => 'DESC', 'post_type' => 'post'); ?>
<?php $query_automatica = array('post__not_in' => array ($post_id), 'posts_per_page'=> '3', 'orderby' => 'rand', 'order' => 'DESC', 'post_type' => 'post'); ?>
<?php if ($relacionados == true) { ?>
    <?php query_posts($query_selecionados); ?>
<?php } else { ?>
    <?php query_posts($query_automatica); ?>
<?php }?>
<?php if (have_posts()) : ?>
    <div id="posts_relacionados">
        <div class="row loop_thumb_post">
            <div class="col-12 mb-3">
                <h3 class="titulo_sessao">Você também vai gostar</h3>
            </div>
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-12">
                    <?php get_template_part( 'template-parts/content', 'post' ); ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>