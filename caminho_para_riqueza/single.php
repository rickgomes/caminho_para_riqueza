<?php get_header(); ?>

<!--BANNER-->
<section id="blog_banner" class="container-fluid d-flex" style="background-image: url(<?php the_post_thumbnail_url('blog_cover'); ?>);">
	<div class="container align-self-center">
		<div class="row">
			<div class="col-12 col-md-8 offset-md-2">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<!--POST-->
<section id="blog" class="container">
	<div class="row my-5">

		<div id="blog_post" class="col-12 col-md-7 col-lg-8">
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumbs" class="mb-3">','</div>'); } ?>

			<time class="data" datetime="<?php echo get_the_date('d/m/Y')?>">
				<?php echo get_the_date('d')?>/<?php echo get_the_date('m')?>/<?php echo get_the_date('Y')?>
			</time>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>

			<!-- CALL TO ACTION -->
			<?php include(locate_template('template-parts/cta/call_to_action.php', false, false )); ?>

			<!-- AUTOR -->
			<div id="author" class="row my-5 mx-0">
				<?php $user_id = get_the_author_meta('ID'); ?>
				<div class="col-2 col-lg-1 px-0">
					<?php echo get_avatar($user_id, $size = '60'); ?>
				</div>
				<div class="infos col-10 col-lg-11">
					<p>Por <b><?php echo get_the_author_meta('display_name'); ?></b></p>
					<p><?php echo get_the_author_meta('user_description'); ?></p>
				</div>
			</div>
			
			<!--TAGS-->
			<div id="tags">
				<?php //the_tags('<h3 class="titulo_sessao">Tags</h3>', ''); ?>
			</div>

			<!-- RELACIONADOS -->
			<?php include(locate_template('template-parts/relacionados/posts.php', false, false )); ?>

			<!--COMENTÁRIOS-->
			<div id="comentarios">
				<h3 class="titulo_sessao mb-3">Comentários</h3>
				<?php comments_template( '', true ); ?>
			</div>
			
		</div>

		<!--SIDEBAR BLOG-->
		<div id="sidebar_blog" class="col-12 col-md-5 col-lg-4 sidebar mt-5 mt-md-0 pt-lg-5">
			<?php dynamic_sidebar('blog_sidebar'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>