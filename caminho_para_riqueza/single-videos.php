<?php get_header(); ?>

<!--VÍDEO-->
<section id="video" class="container">
	<div class="row my-5">

		<div id="video_post" class="col-12">

            <!-- DATA -->
            <time class="data" datetime="<?php echo get_the_date('d/m/Y')?>">
				<?php echo get_the_date('d')?>/<?php echo get_the_date('m')?>/<?php echo get_the_date('Y')?>
			</time>

            <!-- TÍTULO -->
            <h1 class="titulo_post"><?php the_title(); ?></h1>

            <!-- BOX VÍDEO -->
            <div id="box_video" class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php the_field ('youtube'); ?>?rel=0" allowfullscreen></iframe>
            </div>
            
            <!-- TEXTO -->
            <div id="texto_video">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif; ?>			
            </div>

            <!-- RELACIONADOS -->
            <?php include(locate_template('template-parts/relacionados/videos.php', false, false )); ?>

            <!--COMENTÁRIOS-->
            <div id="comentarios">
                <?php comments_template( '', true ); ?>
            </div>

		</div>
	</div>
</section>

<?php get_footer(); ?>