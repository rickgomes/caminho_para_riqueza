<?php get_header(); ?>

    <!-- DESTAQUES -->
    <?php $exibicao = get_field('exibicao_destaques', 'option'); ?>
    <?php $formato_destaques = get_field('formato_destaques', 'option'); ?>
    <?php if ($exibicao == true) { ?>
        <?php if ($formato_destaques == 'Automático') { ?>
            <?php include(locate_template('template-parts/home/destaques.php', false, false )); ?>
        <?php } elseif ($formato_destaques == 'Customizado') { ?>
            <?php include(locate_template('template-parts/home/destaques_custom.php', false, false )); ?>
        <?php } ?>
    <?php } ?>

    <!-- ANUNCÍO BIG -->
    <?php $banner_big = get_field('banner_big', 'option'); ?>
    <?php if (!empty($banner_big)) { ?>
        <?php $banner_big_link = get_field('banner_big_link', 'option'); ?>
        <?php $banner_big_config = get_field('banner_big_config', 'option'); ?>
        <div class="container d-none d-md-block my-5">
            <div class="row">
                <div class="col-12">
                    <?php if ($banner_big_config == true) { $banner_big_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $banner_big_link; ?>" <?php echo $banner_big_config; ?>>
                        <img src="<?php echo $banner_big['url']; ?>" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- ÚLTIMOS VÍDEOS -->
    <?php $exibicao_videos = get_field('exibicao_videos', 'option'); ?>
    <?php if ($exibicao_videos == true) { ?>
        <?php query_posts(array('posts_per_page'=> '3', 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'videos')); ?>
        <?php if (have_posts()) : ?>
            <section id="ultimos_videos" class="container my-5">
                <div class="row loop_thumb">
                    <?php $sessao_videos = get_field('sessao_videos', 'option'); ?>
                    <?php if(!empty($sessao_videos)) { ?>
                        <div class="col-12 mb-3">
                            <h3 class="titulo_sessao"><?php echo $sessao_videos; ?></h3>
                        </div>
                    <?php } ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-12 col-md-4 mb-3 mb-lg-0">
                            <?php get_template_part( 'template-parts/content', 'thumb_video' ); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    <?php } ?>

    <!-- PUBLICAÇÕES FIXAS -->
    <?php $exibicao_fixas = get_field('exibicao_fixas', 'option'); ?>
    <?php if ($exibicao_fixas == true) { ?>
        <?php include(locate_template('template-parts/home/publicacoes_fixas.php', false, false )); ?>
    <?php } ?>

    <!-- BOX NOTÍCIAS & SIDEBAR -->
    <?php $exibicao_ultimas = get_field('exibicao_ultimas', 'option'); ?>
    <?php if ($exibicao_ultimas == true) { ?>
        <?php include(locate_template('template-parts/home/ultimas_publicacoes.php', false, false )); ?>
    <?php } ?>

    <!-- ANUNCÍO RODAPÉ -->
    <?php $banner_rodape = get_field('banner_rodape', 'option'); ?>
    <?php if (!empty($banner_rodape)) { ?>
        <?php $banner_rodape_link = get_field('banner_rodape_link', 'option'); ?>
        <?php $banner_rodape_config = get_field('banner_rodape_config', 'option'); ?>
        <div class="container d-none d-md-block my-5">
            <div class="row">
                <div class="col-12">
                    <?php if ($banner_rodape_config == true) { $banner_rodape_config = 'target="_blank"'; } ?>
                    <a href="<?php echo $banner_rodape_link; ?>" <?php echo $banner_rodape_config; ?>>
                        <img src="<?php echo $banner_rodape['url']; ?>" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    <?php } ?>

<?php get_footer(); ?>