<?php get_header(); ?>

<!--PÁGINA-->
<section id="page" class="container">
	<div id="conteudo_page" class="row my-5">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <!-- TÍTULO -->
            <div class="col-12">
                <h1 class="titulo mb-3"><?php the_title(); ?></h1>
            </div>
            
            <!-- COLUNA 01 -->
            <div id="coluna_01" class="col-12">
                <?php the_content(); ?>
            </div>
        <?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>