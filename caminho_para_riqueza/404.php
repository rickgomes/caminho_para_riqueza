<?php get_header(); ?>

<!--PÁGINA 404-->
<section id="page" class="container">
	<div id="conteudo_page" class="row my-5">
            <!-- TÍTULO -->
            <div class="col-12">
                <h1 class="titulo mb-3">Ops...</h1>
				
            </div>
            
            <!-- COLUNA 01 -->
            <div id="coluna_01" class="col-12">
				<h2 >Não há nada aqui :(</h2>
				<h3 >Mas vamos te ajudar!</h3>
				<br>
				O que deseja fazer? <br><br>
		        
				1) Voltar para a <a href="https://www.caminhoparariqueza.com.br/">PÁGINA INICIAL</a> <br>
				2) Ir para <a href="https://www.caminhoparariqueza.com.br/renda-fixa/">RENDA FIXA</a> <br>
				3) Ir para <a href="https://www.caminhoparariqueza.com.br/renda-variavel-bolsa/">RENDA VARIÁVEL</a> <br>
		   
		    </div>
	</div>
</section>

<?php get_footer(); ?>