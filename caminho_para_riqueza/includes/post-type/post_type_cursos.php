<?php
//Criando as Ações
define("cursos_post_type", 'cursos');
add_action('init', 'cursos_post_type');  

//Registra o Custom Post Type
function cursos_post_type() {
	
	//Cria as labels de exibição do cursos
	$labels = array(
		    'name' => _x('Cursos Online', 'Curso Online'),
		    'singular_name' => _x('Curso Online', 'Curso Online'),
		    'add_new' => _x('Novo Curso Online', 'Destino'),
		    'add_new_item' => __('Adicionar Novo Curso Online'),
		    'edit_item' => __('Editar Curso Online'),
		    'new_item' => __('Novo Curso Online Adicionada'),
		    'view_item' => __('Ver Curso Online'),
		    'search_items' => __('Buscar Curso Online'),
		    'not_found' =>  __('Nenhum Curso Online foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Curso Online foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( cursos_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'curso'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 10,
			 'menu_icon' => 'dashicons-desktop',
	         'supports' => array('title', 'thumbnail')));
}
?>