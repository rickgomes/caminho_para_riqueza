<?php
//Criando as Ações
define("webinars_post_type", 'webinars');
add_action('init', 'webinars_post_type');  

//Registra o Custom Post Type
function webinars_post_type() {
	
	//Cria as labels de exibição do Webinars
	$labels = array(
		    'name' => _x('Webinars', 'Webinar'),
		    'singular_name' => _x('Webinar', 'Webinar'),
		    'add_new' => _x('Novo Webinar', 'Destino'),
		    'add_new_item' => __('Adicionar Novo Webinar'),
		    'edit_item' => __('Editar Webinar'),
		    'new_item' => __('Novo Webinar Adicionada'),
		    'view_item' => __('Ver Webinar'),
		    'search_items' => __('Buscar Webinar'),
		    'not_found' =>  __('Nenhum Webinar foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Webinar foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( webinars_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'webinar'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 10,
			 'menu_icon' => 'dashicons-format-video',
	         'supports' => array('title', 'thumbnail')));
}
?>