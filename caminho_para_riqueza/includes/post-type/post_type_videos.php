<?php
//Criando as Ações
define("videos_post_type", 'videos');
add_action('init', 'videos_post_type');  

//Registra o Custom Post Type
function videos_post_type() {
	
	//Cria as labels de exibição do videos
	$labels = array(
		    'name' => _x('Vídeos', 'Vídeo'),
		    'singular_name' => _x('Video', 'Vídeo'),
		    'add_new' => _x('Novo Vídeo', 'Destino'),
		    'add_new_item' => __('Adicionar Novo Vídeo'),
		    'edit_item' => __('Editar Vídeo'),
		    'new_item' => __('Novo Vídeo Adicionada'),
		    'view_item' => __('Ver Vídeo'),
		    'search_items' => __('Buscar Vídeo'),
		    'not_found' =>  __('Nenhum Vídeo foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Vídeo foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( videos_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'video'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 10,
			 'menu_icon' => 'dashicons-video-alt3',
	         'supports' => array('title', 'editor', 'thumbnail', 'comments')));
}
?>