<?php
//Criando as Ações
define("infograficos_post_type", 'infograficos');
add_action('init', 'infograficos_post_type');  

//Registra o Custom Post Type
function infograficos_post_type() {
	
	//Cria as labels de exibição do infograficos
	$labels = array(
		    'name' => _x('Infográficos', 'Infográfico'),
		    'singular_name' => _x('Infográfico', 'Infográfico'),
		    'add_new' => _x('Novo Infográfico', 'Infográfico'),
		    'add_new_item' => __('Adicionar Novo Infográfico'),
		    'edit_item' => __('Editar Infográfico'),
		    'new_item' => __('Novo Infográfico Adicionada'),
		    'view_item' => __('Ver Infográfico'),
		    'search_items' => __('Buscar Infográfico'),
		    'not_found' =>  __('Nenhum Infográfico foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Infográfico foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( infograficos_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'infografico'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 10,
			 'menu_icon' => 'dashicons-format-image',
	         'supports' => array('title', 'thumbnail')));
}
?>