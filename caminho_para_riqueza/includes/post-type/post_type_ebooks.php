<?php
//Criando as Ações
define("ebooks_post_type", 'ebooks');
add_action('init', 'ebooks_post_type');  

//Registra o Custom Post Type
function ebooks_post_type() {
	
	//Cria as labels de exibição do ebooks
	$labels = array(
		    'name' => _x('E-book', 'E-book'),
		    'singular_name' => _x('E-book', 'E-book'),
		    'add_new' => _x('Novo E-book', 'Destino'),
		    'add_new_item' => __('Adicionar Novo E-book'),
		    'edit_item' => __('Editar E-book'),
		    'new_item' => __('Novo E-book Adicionada'),
		    'view_item' => __('Ver E-book'),
		    'search_items' => __('Buscar E-book'),
		    'not_found' =>  __('Nenhum E-book foi encontrado'),
		    'not_found_in_trash' => __('Nenhum E-book foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( ebooks_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'ebook'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 10,
			 'menu_icon' => 'dashicons-book-alt',
	         'supports' => array('title', 'thumbnail')));
}
?>