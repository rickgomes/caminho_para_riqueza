<?php get_header(); ?>

<!--HOME BLOG-->
<section id="blog_home" class="container">
	<div class="row my-5">
		<!--EXIBIÇÃO DOS POSTS-->
		<div id="home_blog" class="loop_thumb_post col-12 col-md-7 col-lg-8">

			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumbs" class="mb-3">','</div>'); } ?>
			<h1 class="titulo_sessao d-block mb-4"><?php single_cat_title(); ?></h1>

			<?php if (have_posts()) : ?> 
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'post' ); ?>
			<?php endwhile; ?>
			<?php else : ?>
				Ainda não há nenhum conteúdo nesta categoria. 
			<?php endif; ?>

			<!--PÁGINAÇÃO-->
			<?php wp_pagenavi(); ?>
		</div>

		<!--SIDEBAR BLOG-->
		<div id="sidebar_blog" class="col-12 col-md-5 col-lg-4 sidebar mt-5 mt-md-0 pt-lg-5">
			<?php dynamic_sidebar('blog_sidebar'); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>