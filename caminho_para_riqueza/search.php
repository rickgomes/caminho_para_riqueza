<?php get_header(); ?>

<!--BUSCA-->
<section id="pg_blog" class="container">
	<div class="row my-5">
		<div id="conteudo_blog" class="col-12">
        <?php if (have_posts()) : ?>
            <div id="blog_relacionados">
                <div class="row loop_thumb">
                    <div class="col-12">
                        <h1 class="titulo_sessao">
                            Resultado da Pesquisa por: 
                            <span><?php printf(__(' %s'), get_search_query()); ?></span>
                        </h1>
                    </div>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php $post_type_slug = get_post_type($post->ID); ?>
                        <?php $post_type = get_post_type_object($post_type_slug); ?>
                        <?php if ($post_type->labels->name == "Webinars") { ?>
                            <div class="col-12 col-md-4 my-5 mb-lg-0">
                                <?php get_template_part( 'template-parts/content', 'thumb_alt' ); ?>
                            </div>
                        <?php } ?>
                        <?php if ($post_type->labels->name == "E-book") { ?>
                            <div class="col-12 col-md-4 my-5 mb-lg-0">
                                <?php get_template_part( 'template-parts/content', 'thumb_alt' ); ?>
                            </div>
                        <?php } ?>
                        <?php if ($post_type->labels->name == "Infográficos") { ?>
                            <div class="col-12 col-md-4 my-5 mb-lg-0">
                                <?php get_template_part( 'template-parts/content', 'thumb_alt' ); ?>
                            </div>
                        <?php } ?>
                        <?php if ($post_type->labels->name == "Cursos Online") { ?>
                            <div class="col-12 col-md-4 my-5 mb-lg-0">
                                <?php get_template_part( 'template-parts/content', 'thumb_alt' ); ?>
                            </div>
                        <?php } else { ?>
                            <div class="col-12 col-md-4 my-5 mb-lg-0">
                                <?php get_template_part( 'template-parts/content', 'thumb' ); ?>
                            </div>
                        <?php } ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php else : ?>
            <div id="blog_relacionados">
                <div class="row loop_thumb">
                    <div class="col-12">
                        <h1 class="titulo_sessao">
                            Resultado da Pesquisa por: 
                            <span><?php printf(__(' %s'), get_search_query()); ?></span>
                        </h1>
                    </div>
                    <div class="col-12">
                        Nenhum resultado foi encontrado por esta pesquisa.
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!--PÁGINAÇÃO-->
        <?php wp_pagenavi(); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>