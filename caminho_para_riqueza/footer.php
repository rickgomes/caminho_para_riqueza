<footer id="rodape">

     <!-- MENU RODAPÉ -->
    <?php if (is_active_sidebar('box_menu_rodape')) : ?>
        <div id="box_menu" class="container-fluid">
            <div id="menu_rodape" class="container">
                <div class="row py-4">
                    <?php dynamic_sidebar('box_menu_rodape'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>	

    <!-- DESCRIÇÃO -->
    <?php $descricao = get_field('descricao', 'option'); // Descrição ?>
    <?php if (!empty($descricao)) { ?>
        <div id="box_descricao" class="container-fluid">
            <div id="descricao" class="container">
                <div class="row py-3">
                    <div class="col-12">
                        <?php echo $descricao; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- NEWSLETTER E REDES SOCIAIS -->
    <div id="box_news" class="container-fluid">
        <div id="newsletter" class="container">
            <div class="row py-3">
                <div id="form_news" class="col-12 col-md-8">
                    <?php $newsletter = get_field('newsletter_form', 'option'); ?>
                    <?php if (!empty($newsletter)) { ?>
                        <span class="titulo">Newsletter</span> 
                        <p class="desc"><?php the_field ('newsletter_texto', 'option'); ?></p>
                        <?php echo do_shortcode($newsletter); ?>
                    <?php } ?>
                </div>
                <div id="redes_sociais" class="d-flex col-12 col-md-4 flex-column justify-content-end">
                    <div class="d-flex justify-content-end">
                        <?php $instagram = get_field('instagram', 'option'); // Redes Sociais - Instagram ?>
                        <?php $facebook  = get_field('facebook', 'option'); //  Redes Sociais - Facebook ?>
                        <?php $youtube   = get_field('youtube', 'option'); //   Redes Sociais - YouTube ?>
                        <?php $twitter   = get_field('twitter', 'option'); //   Redes Sociais - Twitter ?>
                        <?php $whatsapp  = get_field('whatsapp', 'option'); //  Redes Sociais - WhatsApp ?>
                        <?php if (!empty($instagram)) { ?>
                            <a href="<?php echo $instagram; ?>" alt="Instagram" target="_blank" class="item ml-3"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <?php } ?>
                        <?php if (!empty($facebook)) { ?>
                            <a href="<?php echo $facebook; ?>" alt="Facebook" target="_blank" class="item ml-3"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <?php } ?>
                        <?php if (!empty($youtube)) { ?>
                            <a href="<?php echo $youtube; ?>" alt="Youtube" target="_blank" class="item ml-3"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                        <?php } ?>
                        <?php if (!empty($twitter)) { ?>
                            <a href="<?php echo $twitter; ?>" alt="Twitter" target="_blank" class="item ml-3"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <?php } ?>
                        <?php if (!empty($whatsapp)) { ?>
                            <a href="http://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo $whatsapp; ?>" alt="WhatsApp" target="_blank" class="item ml-3"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DIREITOS E ASSINATURA -->
    <div id="box_direitos" class="container-fluid">
        <div id="direitos" class="container">
            <div class="row py-2">
                <div class="col-12 col-md-6 mb-3 mb-md-0 text-center text-md-left">
                    Caminho para Riqueza - <?php echo date('Y'); ?> - Todos os direitos reservados.
                </div>
                <div class="col-12 col-md-6 text-center text-md-right">
                    <a href="http://manymakers.digital" class="link" target="_blank">Many Makers</a>
                </div>
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>
</body>
</html>