<?php  /* Template Name: 02 Colunas */ ?>

<?php get_header(); ?>

<!--PÁGINA-->
<section id="page" class="container">
	<div id="conteudo_page" class="row my-5">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <!-- TÍTULO -->
            <div class="col-12">
                <h1 class="titulo mb-3"><?php the_title(); ?></h1>
            </div>
            
            <!-- COLUNA 01 -->
            <div id="coluna_01" class="col-12 col-md-6">
                <?php the_content(); ?>
            </div>

            <!-- COLUNA 02 -->
            <div id="coluna_02" class="col-12 col-md-6">
                <?php the_field('coluna_02'); ?>
            </div>
        <?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>