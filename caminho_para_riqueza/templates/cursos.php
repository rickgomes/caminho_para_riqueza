<?php  /* Template Name: Cursos Online */ ?>

<?php get_header(); ?>

<!--PÁGINA DE CURSOS ONLINE-->
<section id="pg_cursos_online" class="container">
	<div class="row my-5">
		<div id="conteudo_cursos" class="col-12">
        <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
        <?php query_posts(array('posts_per_page'=> '12', 'paged' => $paged, 'orderby' => 'date', 'order' => 'DESC', 'post_type' => 'cursos')); ?>
        <?php if (have_posts()) : ?>
            <div id="cursos_relacionados">
                <div class="row loop_thumb">
                    <div class="col-12">
                        <h1 class="titulo_sessao">Cursos Online</h1>
                    </div>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-12 col-md-4 my-5 mb-lg-0">
                            <?php get_template_part( 'template-parts/content', 'thumb_alt' ); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php else : ?>
            <div id="cursos_relacionados">
                <div class="row loop_thumb">
                    <div class="col-12">
                        <h1 class="titulo_sessao">Cursos Online</h1>
                    </div>
                    <div class="col-12">
                        Ainda não há nenhum conteúdo nesta sessão.
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!--PÁGINAÇÃO-->
        <?php wp_pagenavi(); ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>