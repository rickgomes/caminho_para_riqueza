<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="pt-br">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Many Makers | manymakers.digital">

    <!--META DESCRIPTION-->
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">

    <!--GOOGLE FONTS-->
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:700,900|Montserrat:400,400i,700,700i" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>

  <body>

  <?php $logo = get_field('logo', 'option'); // Logo ?>
  <?php $instagram = get_field('instagram', 'option'); // Redes Sociais - Instagram ?>
  <?php $facebook  = get_field('facebook', 'option'); //  Redes Sociais - Facebook ?>
  <?php $youtube   = get_field('youtube', 'option'); //   Redes Sociais - YouTube ?>
  <?php $twitter   = get_field('twitter', 'option'); //   Redes Sociais - Twitter ?>
  <?php $whatsapp  = get_field('whatsapp', 'option'); //  Redes Sociais - WhatsApp ?>

  <!-- CABEÇALHO -->
  <header id="cabecalho" class="mt-lg-4">
    <div id="topo" class="container d-none d-lg-block">
      <div class="row">
        <div class="col-12 col-lg-3">
          <a href="https://www.caminhoparariqueza.com.br">
            <img src="<?php echo $logo['url']; ?>" alt="Caminho para Riqueza">
          </a>
        </div>
        <div class="col-12 col-lg-9 d-flex flex-column justify-content-end">
          <div id="busca" class="d-flex justify-content-end">
            <form id="form_busca" action="/" method="get" class="form-inline" role="search" accept-charset="utf-8">
                <input type="text" class="form-control" name="s" id="s" value="<?php the_search_query(); ?>" />
                <button id="botao" class="btn" type="submit"></button>
            </form>
          </div>
          <div id="redes_sociais" class="d-flex justify-content-end my-1">
            <?php if (!empty($instagram)) { ?>
              <a href="<?php echo $instagram; ?>" alt="Instagram" target="_blank" class="item ml-3"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <?php } ?>
            <?php if (!empty($facebook)) { ?>
              <a href="<?php echo $facebook; ?>" alt="Facebook" target="_blank" class="item ml-3"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
            <?php } ?>
            <?php if (!empty($youtube)) { ?>
              <a href="<?php echo $youtube; ?>" alt="Youtube" target="_blank" class="item ml-3"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
            <?php } ?>
            <?php if (!empty($twitter)) { ?>
              <a href="<?php echo $twitter; ?>" alt="Twitter" target="_blank" class="item ml-3"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
            <?php } ?>
            <?php if (!empty($whatsapp)) { ?>
              <a href="http://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo $whatsapp; ?>" alt="WhatsApp" target="_blank" class="item ml-3"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
            <?php } ?>
          </div>

          <div class="d-flex justify-content-end">
            <?php $menu_topo = array(
              'menu'    => 'menu_topo',
              'theme_location' => 'menu_topo',
              'depth'    => 1,
              'container_id'   => 'menu_topo',
              'container_class'   => '',
              'menu_class'   => 'nav',
              'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
              'walker'    => new wp_bootstrap_navwalker()
              );
              wp_nav_menu($menu_topo);
            ?>
          </div>
        </div>
      </div>
    </div>

    <!-- MENU PRINCIPAL -->
    <nav id="menu" class="navbar sticky-top navbar-expand-lg mt-lg-3">
      <div id="menu_container" class="container">
        <a class="navbar-brand d-lg-none" href="<?php echo site_url(''); ?>">
          <img src="<?php echo $logo['url']; ?>" class="img-fluid" alt="Caminho para Riqueza">
        </a>
        <button id="bt_mobile" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_principal" aria-controls="menu_principal" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="menu_principal" class="collapse navbar-collapse">
          <?php $menu_principal = array(
            'menu'    => 'menu_principal',
            'theme_location' => 'menu_principal',
            'depth'    => 4,
            'container'   => '',
            'menu_class'   => 'navbar-nav nav-fill w-100',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'    => new wp_bootstrap_navwalker()
            );
            wp_nav_menu($menu_principal);
          ?>

          <div class="d-flex flex-column d-lg-none">
            <div id="busca" class="d-flex justify-content-start mt-3">
              <form id="form_busca" action="/" method="get" class="form-inline w-100 d-lg-none" role="search" accept-charset="utf-8">
                  <input type="text" class="form-control" placeholder="Busca" name="s" id="s" value="<?php the_search_query(); ?>" />
                  <button id="botao" class="btn" type="submit"></button>
              </form>
            </div>

            <div id="redes_sociais" class="d-flex justify-content-start mt-3">
              <?php if (!empty($instagram)) { ?>
                <a href="<?php echo $instagram; ?>" alt="Instagram" target="_blank" class="item mr-2"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              <?php } ?>
              <?php if (!empty($facebook)) { ?>
                <a href="<?php echo $facebook; ?>" alt="Facebook" target="_blank" class="item mr-2"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
              <?php } ?>
              <?php if (!empty($youtube)) { ?>
                <a href="<?php echo $youtube; ?>" alt="Youtube" target="_blank" class="item mr-2"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
              <?php } ?>
              <?php if (!empty($twitter)) { ?>
                <a href="<?php echo $twitter; ?>" alt="Twitter" target="_blank" class="item mr-2"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
              <?php } ?>
              <?php if (!empty($whatsapp)) { ?>
                <a href="http://api.whatsapp.com/send?1=pt_BR&phone=55<?php echo $whatsapp; ?>" alt="WhatsApp" target="_blank" class="item mr-2"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
              <?php } ?>
            </div>

            <div id="menu_mobile_topo" class="my-3">
              <?php $menu_topo = array(
                'menu'    => 'menu_topo',
                'theme_location' => 'menu_topo',
                'depth'    => 1,
                'container'   => nav,
                'container_id'   => 'menu_topo',
                'container_class'   => '',
                'menu_class'   => 'nav flex-column',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'    => new wp_bootstrap_navwalker()
                );
                wp_nav_menu($menu_topo);
              ?>
            </div>
          </div>

        </div>
      </div>
    </nav>

    <!-- BARRA TRANDING VIEW NA HOME -->
    <?php if (is_home()) { ?>
      <div id="barra_tv">
        <?php the_field('tranding_view', 'option'); ?>
      </div>
    <?php } ?>
  </header>