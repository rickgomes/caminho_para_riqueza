"use strict";

const autoprefixer = require("autoprefixer");
const browsersync = require("browser-sync").create();
const cssnano = require("cssnano");
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const sourcemaps = require('gulp-sourcemaps')

function browserSync(done) {
    browsersync.init({
        proxy: "localhost"
    });
    done();
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function css() {
    return gulp
        .src("./caminho_para_riqueza/sass/style.scss")
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest("./caminho_para_riqueza/css/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("./caminho_para_riqueza/css/"))
        .pipe(browsersync.stream());
}

function css_admin() {
    return gulp
        .src("./caminho_para_riqueza/sass/admin/admin.scss")
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest("./caminho_para_riqueza/css/admin"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("./caminho_para_riqueza/css/admin"))
        .pipe(browsersync.stream());
}

function watchFiles() {
    gulp.watch("./caminho_para_riqueza/sass/**/*",  css);
    gulp.watch("./caminho_para_riqueza/sass/admin/*",  css_admin);
    gulp.watch("./caminho_para_riqueza/**/*", gulp.series(browserSyncReload));
}

const watch = gulp.parallel(watchFiles, browserSync);
const build = gulp.series(gulp.parallel(css, css_admin, watch));

exports.css = css;
exports.css_admin = css_admin;
exports.watch = watch;
exports.build = build;
exports.default = build;